package com.panopto.www.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
 
import com.panopto.www.config.ApiUrl;
import com.panopto.www.config.PanoptoAuth;
import com.panopto.www.config.PreFixs;
import com.panopto.www.model.Folder;
import com.panopto.www.model.JqTree;
import com.panopto.www.model.RemoteRecorder;
import com.panopto.www.model.Result;
import com.panopto.www.model.ResultArray;
import com.panopto.www.model.ScheduledRecordingInfo;
import com.panopto.www.model.Session;
import com.panopto.www.model.User;
 

@Controller
public class PanoptoServiceController {
	
	private static Logger logger = LoggerFactory.getLogger(PanoptoServiceController.class);

	
	@Autowired
	private PanoptoAuth panoptoAuth;
	@Autowired
	private ApiUrl apiUrl;
	
	
	@Autowired
	private PreFixs prefix;
	
	@Autowired
	private RestTemplate restTemplate;
	
	private String launchUrl = "";
	
	@PostConstruct
	public void init() {
		logger.info("START");
		HashMap<String, Object> params = new HashMap<String, Object>();
		ResponseEntity<ResultArray> ret = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getLauncherUrl()), params, ResultArray.class);
		launchUrl = ret.getBody().getObj().toString();
	}
	
	@RequestMapping(value = "/", method= {RequestMethod.GET, RequestMethod.POST})
	public String autoRedirect() {
		return "redirect:/pt/panopto_video_list";
	}
	
	
	@RequestMapping(value = "/next", method= {RequestMethod.GET, RequestMethod.POST})
	public String next(
			@RequestParam(value="content_id", required=false, defaultValue="") String content_id,
			@RequestParam(value="title", required=false, defaultValue="") String title,
			@RequestParam(value="content", required=false, defaultValue="") String content,
			@RequestParam(value="duration", required=false, defaultValue="") String duration,
			@RequestParam(value="user_name", required=false, defaultValue="") String user_name,
			@RequestParam(value="view_url", required=false, defaultValue="") String view_url
			) { 
		System.out.println(content_id);
		System.out.println(title);
		System.out.println(content);
		System.out.println(duration);
		System.out.println(user_name);
		System.out.println(view_url);
		return "redirect:/pt/panopto_video_list";
	}
	
	
	
	 
	@RequestMapping(value = "/pt/panopto_video_list", method= {RequestMethod.GET, RequestMethod.POST})
    public String panopto_video_list(Principal principal, Model model,
    		@RequestParam(value="alert", required=false, defaultValue="") String alert,
    		@RequestParam(value="API_KEY", required=false, defaultValue="") String API_KEY,
    		@RequestParam(value="USER_ID", required=false, defaultValue="") String USER_ID,
    		@RequestParam(value="return_url", required=false, defaultValue="") String return_url,
    		HttpServletRequest request
            , HttpServletResponse response
    		) throws IOException {
		
		//kr_13989badc8f73855bb877c998dr799274qw
		//hufs_189bdc8f7855bb77c98d79qwmsl93aj7dox829
		//svctop_1389adcf7385bb877998d7997qwabkd98kj
		if(!API_KEY.equals("svctop_1389adcf7385bb877998d7997qwabkd98kj") || USER_ID.equals("") || return_url.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.println("<script> window.open('', '_self').close();  </script>");
			out.flush();
		}
		System.out.println(launchUrl);
		Date today = new Date();
		String dateFormat = DateFormatUtils.format(today, "yyyy-MM-dd");
		
		/*
		 * folderOwnerId 에 로그인 아이디를 넣어서 해당 사용자의 폴더데이터만 가져온다.
		 */
		int pageSize = 1000;
		
		USER_ID = prefix.getPrefix()+USER_ID;
		
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("currentPage", "1");
		params.add("page_size", pageSize);
		params.add("searchQuery", USER_ID);
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		
		ResponseEntity<ResultArray> retF = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getFolderList()), params, ResultArray.class);
		List<Folder> fList = retF.getBody().getFolders();
		
		List<JqTree> folders = new ArrayList<JqTree>(); 
		if(fList!=null && fList.size()>0) {
			for(int i=0; i<fList.size(); i++) {
				Folder f = (Folder)fList.get(i);
				JqTree inDt = new JqTree();
				inDt.setLabel(f.getName());
				inDt.setValue(f.getId());
				if(f.getChildFolders().length > 0) {
					List<JqTree> subfolders = new ArrayList<JqTree>(); 
					inDt.setExpanded(false);
					JqTree items = new JqTree();
					items.setValue(String.join(",", f.getChildFolders()));
					items.setLabel("Loading...");
					items.setItems(null);
					subfolders.add(items);
					inDt.setItems(subfolders);
				} else {
					inDt.setExpanded(false);
				}
				folders.add(inDt);
			}
		}
		
		int pageSizes = 1000;
		MultiValueMap<String, Object> paramss = new LinkedMultiValueMap<String, Object>();
		paramss.add("currentPage", "1");
		paramss.add("page_size", pageSizes);
		paramss.add("sort", "Name");
		paramss.add("userkey", panoptoAuth.getAuthKey());
		paramss.add("userpwd", panoptoAuth.getAuthPwd());
		ResponseEntity<ResultArray> ret = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getRecordingList()), paramss, ResultArray.class);
		System.err.println(ret.getBody().toString());
		List<RemoteRecorder> remoteRecorder = ret.getBody().getRemoteRecorder();
		
		
		System.err.println(folders.toString());
		model.addAttribute("return_url",return_url);
		model.addAttribute("folderOwnerId",USER_ID);
		model.addAttribute("launchUrl", launchUrl);
		model.addAttribute("folders", folders);
//		model.addAttribute("alert", alert);
		
		
		model.addAttribute("remoteRecorder", remoteRecorder);
		model.addAttribute("startDate", dateFormat);
        return "panopto/panopto_video_list";
    }
	
	
	
	
	
	@RequestMapping(value = "/pt/user_info_by_userId", method= {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody List<User> userInfoByUserId(
    		@RequestParam(value="creatorId", required=true) String creatorId
    		) {
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("userIds", creatorId);
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
		HttpEntity<MultiValueMap<String, Object>> data = new HttpEntity<MultiValueMap<String,Object>>(params, headers);
		
		ResponseEntity<ResultArray> rets = restTemplate.postForEntity
				(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getUserInfoByUserId()), data, ResultArray.class);
		System.err.println(rets.getBody().toString());
		List<User> results = rets.getBody().getUser();	
		return results;//임시로 넣은값..
    }
	
	
	
	
	
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	
	
	
	
	@RequestMapping(value = "/pt/schedule_list", method= {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
	public HashMap<String, Object> schedulelist(
    		Model model,
    		@RequestParam(value="currentPage", required=false, defaultValue="1") int currentPage,
    		@RequestParam(value="folderOwnerId", required=false, defaultValue="") String folderOwnerId,
    		@RequestParam(value="folderId", required=false, defaultValue="") String folderId,
    		@RequestParam(value="sessionName", required=false, defaultValue="") String sessionName,
    		@RequestParam(value="sessionState", required=false, defaultValue="111111") String sessionState,
    		@RequestParam(value="startDate", required=false, defaultValue="") String startDate,
    		@RequestParam(value="endDate", required=false, defaultValue="") String endDate,
    		@RequestParam(value="sort", required=false, defaultValue="Date") String sort,
    		//
    		@RequestParam(value="recordstartindex", required=false, defaultValue="")int recordstartindex,
    		@RequestParam(value="pagenum", required=false, defaultValue="1")int pagenum,
    		@RequestParam(value="pagesize", required=false, defaultValue="1")int pagesize
    		
    		) throws ParseException{
		int pageSize = pagesize;

		String sCurTime = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA).format(new Date());
		
		if(startDate.equals("")) {//5,7
			startDate = sCurTime;
			endDate = sCurTime;
			String str_startDate = startDate.substring(5,7);
			if(str_startDate.equals("01")) {
				String yyyy_startDate = startDate.substring(0,4);
				int int_yyyy_startDate = Integer.parseInt(yyyy_startDate);
				int_yyyy_startDate = int_yyyy_startDate - 1 ;
				startDate = String.valueOf(int_yyyy_startDate+"-07-01");
			}else if(str_startDate.equals("02")) {
				String yyyy_startDate = startDate.substring(0,4);
				int int_yyyy_startDate = Integer.parseInt(yyyy_startDate);
				int_yyyy_startDate = int_yyyy_startDate - 1 ;
				startDate = String.valueOf(int_yyyy_startDate+"-08-01");
			}else if(str_startDate.equals("03")) {
				String yyyy_startDate = startDate.substring(0,4);
				int int_yyyy_startDate = Integer.parseInt(yyyy_startDate);
				int_yyyy_startDate = int_yyyy_startDate - 1 ;
				startDate = String.valueOf(int_yyyy_startDate+"-09-01");
			}else if(str_startDate.equals("04")) {
				String yyyy_startDate = startDate.substring(0,4);
				int int_yyyy_startDate = Integer.parseInt(yyyy_startDate);
				int_yyyy_startDate = int_yyyy_startDate - 1 ;
				startDate = String.valueOf(int_yyyy_startDate+"-10-01");
			}else if(str_startDate.equals("05")) {
				String yyyy_startDate = startDate.substring(0,4);
				int int_yyyy_startDate = Integer.parseInt(yyyy_startDate);
				int_yyyy_startDate = int_yyyy_startDate - 1 ;
				startDate = String.valueOf(int_yyyy_startDate+"-11-01");
			}else if(str_startDate.equals("06")) {
				String yyyy_startDate = startDate.substring(0,4);
				int int_yyyy_startDate = Integer.parseInt(yyyy_startDate);
				int_yyyy_startDate = int_yyyy_startDate - 1 ;
				startDate = String.valueOf(int_yyyy_startDate+"-12-01");
			}else if(str_startDate.equals("07")) {
				String yyyy_startDate = startDate.substring(0,4);
				int int_yyyy_startDate = Integer.parseInt(yyyy_startDate);
				startDate = String.valueOf(int_yyyy_startDate+"-01-01");
			}else if(str_startDate.equals("08")) {
				String yyyy_startDate = startDate.substring(0,4);
				int int_yyyy_startDate = Integer.parseInt(yyyy_startDate);
				startDate = String.valueOf(int_yyyy_startDate+"-02-01");
			}else if(str_startDate.equals("09")) {
				String yyyy_startDate = startDate.substring(0,4);
				int int_yyyy_startDate = Integer.parseInt(yyyy_startDate);
				startDate = String.valueOf(int_yyyy_startDate+"-03-01");
			}else if(str_startDate.equals("10")) {
				String yyyy_startDate = startDate.substring(0,4);
				int int_yyyy_startDate = Integer.parseInt(yyyy_startDate);
				startDate = String.valueOf(int_yyyy_startDate+"-04-01");
			}else if(str_startDate.equals("11")) {
				String yyyy_startDate = startDate.substring(0,4);
				int int_yyyy_startDate = Integer.parseInt(yyyy_startDate);
				startDate = String.valueOf(int_yyyy_startDate+"-05-01");
			}else if(str_startDate.equals("12")) {
				String yyyy_startDate = startDate.substring(0,4);
				int int_yyyy_startDate = Integer.parseInt(yyyy_startDate);
				startDate = String.valueOf(int_yyyy_startDate+"-06-01");
			}
		}
		
		
		
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		if(!startDate.equals("")) params.add("startDate", startDate.replaceAll("-", "")+"000000");
		if(!endDate.equals("")) params.add("endDate", endDate.replaceAll("-", "")+"235959");
		params.add("currentPage", pagenum+1);
		params.add("page_size", pageSize);
		//kr-panopto\\\\jkim
		params.add("folderOwnerId", folderOwnerId);
		params.add("folderId", folderId);
		/*
		 * sessionName, searchQuery 에는 항상 같은 검색어를 넣어준다.
		 */

		params.add("sessionName", sessionName);
		params.add("searchQuery", sessionName);
		params.add("sessionState", sessionState);
		params.add("sort", sort);
		params.add("sortIncreasing", "Desc");
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		
		/*
		 * 한글검색 시 인코딩 설정하기
		 */
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
		HttpEntity<MultiValueMap<String, Object>> data = new HttpEntity<MultiValueMap<String,Object>>(params, headers);
		
		ResponseEntity<ResultArray> ret = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleList()), data, ResultArray.class);
		System.err.println(ret.getBody().toString());
		utils.PagingAction page = new utils.PagingAction(
				currentPage, 
				ret.getBody().getTotal(), 
				pageSize,
				10, 
				"searchFrm", 
				"");
		HashMap<String, Object> param1 = new HashMap<>();
		List<Session> result = ret.getBody().getSessions();
		int total = ret.getBody().getTotal();
		if(total>0) {
			for(int i=0; i<result.size(); i++) {
				result.get(i).setNum(total-i-recordstartindex);
				Calendar c = result.get(i).getStartTime();
				String a = c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DATE)+" "+(c.get(Calendar.HOUR_OF_DAY)+9)+":"+c.get(Calendar.MINUTE);
				result.get(i).setStrStartTime(a);
				
				String str = result.get(i).getDuration();
				if(str!=null) {
					String durations = str.replace(".","/");
					String[] sp_str = durations.split("/");
					result.get(i).setDuration_real(sp_str[0]);
					int int_duration = Integer.parseInt(sp_str[0]);
					int min,hour;
					min = int_duration/60;
					hour = min/60;
					int_duration = int_duration % 60;
					min = min % 60;
					String last_duration = "";
					if(hour!=0) {
						last_duration += hour+"시 ";
					}
					if(min!=0) {
						last_duration += min+"분 ";
					}
					last_duration += int_duration+"초";
					result.get(i).setDuration(last_duration);
				}else {
					result.get(i).setDuration("");
				}
				result.get(i).setGoviewerUrl(result.get(i).getViewerUrl().replace("Viewer", "Embed")+"&v=1&offerviewer=false");;
				result.get(i).setViewerUrl(result.get(i).getViewerUrl().substring(0, result.get(i).getViewerUrl().indexOf("/Panopto")) + result.get(i).getThumbUrl());
				HashMap<String, Object> state = (HashMap<String, Object>) result.get(i).getState();
				String str_state = (String) state.get("value");
				if(str_state.equals("Processing")) {//
					result.get(i).setUploadBt("<button class=\"btn btn-default refresh_btn\" id=\"refresh_bt\"><span class=\"glyphicon glyphicon-refresh\"></span><p>새로고침</p></button>");
					result.get(i).setName("[처리중] "+result.get(i).getName());
					result.get(i).setViewerUrl("/resource/assets/img/process_thumb.png");
				}else {
					result.get(i).setUploadBt("");
				}
			}
		}
		
		param1.put("total", total);
		param1.put("Rows", result);
        return param1;
    }
	
	
	@RequestMapping(value = "/pt/schedule_edit_name", method= {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody HashMap<String, Object> scheduleEditName(
    		@RequestParam(value="id", required=true) String id,
    		@RequestParam(value="name", required=true) String name
    		) {
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		System.out.println(name);
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("sessionId", id);
		params.add("name", name);
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		
		ResponseEntity<ResultArray> ret = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleEditName()), params, ResultArray.class);
		System.err.println(ret.getBody().toString());

		result.put("isBool", ret.getBody().isBoolResult());
		result.put("alert", ret.getBody().getMsg());
		
		return result;
    }
	
	@RequestMapping(value = "/pt/schedule_edit_desc", method= {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody HashMap<String, Object> scheduleDditDesc(
    		@RequestParam(value="id", required=true) String id,
    		@RequestParam(value="content", required=true) String content
    		) {
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		System.out.println(content);
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("sessionId", id);
		params.add("description", content);
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		
		ResponseEntity<ResultArray> ret = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleEditDesc()), params, ResultArray.class);
		System.err.println(ret.getBody().toString());

		result.put("isBool", ret.getBody().isBoolResult());
		result.put("alert", ret.getBody().getMsg());
		
		return result;
    }
	
	
	
	
	

	@RequestMapping(value = "/pt/delete_schedule", method= {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody HashMap<String, Object> deleteSchedule(
    		@RequestParam(value="id", required=true) String id
    		) {
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("id", id);
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		
		ResponseEntity<ResultArray> ret = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleDelete()), params, ResultArray.class);
		System.err.println(ret.getBody().toString());
		result.put("isBool", ret.getBody().isBoolResult());
		result.put("alert", ret.getBody().getMsg());
		
		return result;
    }

	@RequestMapping(value = "/pt/schedule_view", method= {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody HashMap<String, Object> scheduleView(
    		@RequestParam(value="id", required=true) String id
    		) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("id", id);
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		
		ResponseEntity<ResultArray> retView = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleView()), params, ResultArray.class);
		System.err.println(retView.getBody().toString());

		result.put("isBool", retView.getBody().isBoolResult());
		if(retView.getBody().isBoolResult()) {
			result.put("session", retView.getBody().getSession().get(0));
		} else {
			result.put("session", null);
		}
		result.put("alert", retView.getBody().getMsg());
		
        return result;
    }
    
	@RequestMapping(value = "/pt/schedule_a_meeting", method= {RequestMethod.GET, RequestMethod.POST})
    public String scheduleAMeeting(Principal principal, Model model,
    		@RequestParam(value="alert", required=false, defaultValue="") String alert,
    		@RequestParam(value="folderOwnerId", required=false, defaultValue="") String folderOwnerId
    		) {
		/*
		 * folderOwnerId 에 로그인 아이디를 넣어서 해당 사용자의 폴더데이터만 가져온다.
		 */
		Date today = new Date();
		String dateFormat = DateFormatUtils.format(today, "yyyy-MM-dd");
		
		int pageSize = 1000;
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("currentPage", "1");
		params.add("page_size", pageSize);
		params.add("sort", "Name");
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		
		ResponseEntity<ResultArray> ret = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getRecordingList()), params, ResultArray.class);
		System.err.println(ret.getBody().toString());
		List<RemoteRecorder> remoteRecorder = ret.getBody().getRemoteRecorder();
		
		params.clear();

		params.add("currentPage", "1");
		params.add("page_size", pageSize);
		params.add("searchQuery", folderOwnerId);
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		
		ResponseEntity<ResultArray> retF = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getFolderList()), params, ResultArray.class);
		List<Folder> fList = retF.getBody().getFolders();
		
		List<JqTree> folders = new ArrayList<JqTree>(); 
		if(fList!=null && fList.size()>0) {
			for(int i=0; i<fList.size(); i++) {
				Folder f = (Folder)fList.get(i);
				JqTree inDt = new JqTree();
				inDt.setLabel(f.getName());
				inDt.setValue(f.getId());
				if(f.getChildFolders().length > 0) {
					List<JqTree> subfolders = new ArrayList<JqTree>(); 
					inDt.setExpanded(false);
					JqTree items = new JqTree();
					items.setValue(String.join(",", f.getChildFolders()));
					items.setLabel("Loading...");
					items.setItems(null);
					subfolders.add(items);
					inDt.setItems(subfolders);
				} else {
					inDt.setExpanded(false);
				}
				folders.add(inDt);
			}
		}

		System.err.println(folders.toString());
		model.addAttribute("launchUrl", launchUrl);
		model.addAttribute("remoteRecorder", remoteRecorder);
		model.addAttribute("startDate", dateFormat);
		model.addAttribute("folders", folders);
		model.addAttribute("alert", alert);
		
        return "panopto/schedule_a_meeting_panopto";
    }
    
	@RequestMapping(value = "/pt/subFolder", method= {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody List<JqTree> subFolder(
    		@RequestParam(value="value", required=true) List<String> folderIds) {
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("folderIds", String.join(",", folderIds.toArray(new String[folderIds.size()])));
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		
		ResponseEntity<ResultArray> retF = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getFolderListById()), params, ResultArray.class);
		List<Folder> fList = retF.getBody().getFolder();
		System.err.println(retF.getBody().toString());
		
		List<JqTree> folders = new ArrayList<JqTree>(); 
		
		if(fList!=null && fList.size()>0) {
			for(int i=0; i<fList.size(); i++) {
				Folder f = (Folder)fList.get(i);
				JqTree inDt = new JqTree();
				inDt.setLabel(f.getName());
				inDt.setValue(f.getId());
				if(f.getChildFolders().length > 0) {
					List<JqTree> subfolders = new ArrayList<JqTree>();
					inDt.setExpanded(false);
					JqTree items = new JqTree();
					items.setValue(String.join(",", f.getChildFolders()));
					items.setLabel("Loading...");
					items.setItems(null);
					subfolders.add(items);
					inDt.setItems(subfolders);
				} else {
					inDt.setExpanded(false);
				}
				folders.add(inDt);
			}
		}
		
        return folders;
    }
 
	
	//ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ
	
	@RequestMapping(value = "/pt/schedule_a_meeting_save", method= {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody HashMap<String, Object> scheduleAMeetingSave(
    		@RequestParam(value="name", required=true, defaultValue="") String name,
    		@RequestParam(value="folderId", required=true, defaultValue="") String folderId,
    		@RequestParam(value="description", required=true, defaultValue="") String description,
    		@RequestParam(value="recorderMain", required=true, defaultValue="") String recorderMain,
    		@RequestParam(value="recorderSub", required=false, defaultValue="") String recorderSub,
    		@RequestParam(value="startDate", required=true, defaultValue="") String startDate,
    		@RequestParam(value="startTime", required=true, defaultValue="") String startTime,
    		@RequestParam(value="endTime", required=true, defaultValue="") String endTime,
    		@RequestParam(value="daysOfWeekState", required=false) List<String> daysOfWeekState,
    		@RequestParam(value="daysOfWeekStateEndDate", required=true, defaultValue="") String daysOfWeekStateEndDate,
    		@RequestParam(value="isBroadcast", required=true, defaultValue="false") String isBroadcast
    		) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("name", name);
		params.add("folderId", folderId);
		params.add("recorderMain", recorderMain);
		params.add("recorderSub", recorderSub);
		Locale locale = Locale.KOREA;
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd a KK:mm").withLocale(locale);
		DateTime startDateTime = DateTime.parse(startDate+" "+startTime, formatter);
		Calendar startCal = startDateTime.toCalendar(locale);
		Calendar endCal = startDateTime.plusMinutes(Integer.parseInt(endTime)).toCalendar(locale);
		Date sDate = startCal.getTime();
		Date eDate = endCal.getTime();
		SimpleDateFormat parser = new SimpleDateFormat("yyyyMMddHHmmss");
		params.add("startTime", parser.format(sDate));
		params.add("endTime", parser.format(eDate));
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		
		String addMsg = "";
		
		/*
		 * 한글등록 시 인코딩 설정하기
		 */
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
		HttpEntity<MultiValueMap<String, Object>> data = new HttpEntity<MultiValueMap<String,Object>>(params, headers);
		
		ResponseEntity<ResultArray> retF = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getRecordingCreate()), data, ResultArray.class);
		System.err.println("session create=" + retF.getBody().toString());

		// 이미 동일한 예약이 있다면 true 를 반환한다.
		if(retF.getBody().isConflictsExist()) {
			List<ScheduledRecordingInfo> ConflictsExistList = retF.getBody().getSheduledRecordingInfo();
			if(ConflictsExistList != null && ConflictsExistList.size()>0) {
				addMsg+="[이미 중복되어 생성되지 않는 세션 정보입니다.]\n";
				for(int i=0; i<ConflictsExistList.size(); i++) {
					ScheduledRecordingInfo scheduledRecordingInfo = (ScheduledRecordingInfo)ConflictsExistList.get(i);
					addMsg+="sessionName=" + scheduledRecordingInfo.getSessionName() + " sessionID=" + scheduledRecordingInfo.getSessionID();
					addMsg+="\n";
				}
				addMsg+="[여기까지]\n";
			}
			result.put("session", null);
			result.put("isBool", false);
			result.put("alert", addMsg);
		} else {
			
			if(!retF.getBody().isBoolResult()) {
				result.put("isBool", false);
				result.put("alert", retF.getBody().getMsg());
			} else {
				String newSessionId = retF.getBody().getSessionIDs().get(0);
				params.clear();
	
				params.add("sessionId", newSessionId);
				params.add("isBroadcast", isBroadcast);
				params.add("userkey", panoptoAuth.getAuthKey());
				params.add("userpwd", panoptoAuth.getAuthPwd());
				System.err.println(params.toString());
				ResponseEntity<String> retIsBroadCast = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleEditIsBroadcast()), params, String.class);
				System.err.println(retIsBroadCast.getBody().toString());
				
				if(!description.equals("")) {
					params.clear();
					data = null;
	
					params.add("sessionId", newSessionId);
					params.add("description", description);
					params.add("userkey", panoptoAuth.getAuthKey());
					params.add("userpwd", panoptoAuth.getAuthPwd());
					System.err.println(params.toString());
					
					data = new HttpEntity<MultiValueMap<String,Object>>(params, headers);
					
					ResponseEntity<String> retDesc = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleEditDesc()), data, String.class);
					System.err.println(retDesc.getBody().toString());
				}
				
				// 반복설정을 안할 수도 있다.
				if(!daysOfWeekStateEndDate.equals("")) {
					params.clear();
					data = null;
					StringBuilder daysOfWeekStateStr = new StringBuilder("0000000");
					if(daysOfWeekState != null && daysOfWeekState.size()>0) {
						for(int i=0; i<daysOfWeekState.size(); i++) {
							if(daysOfWeekState.get(i).equals("1")) {
								daysOfWeekStateStr.setCharAt(0, '1');
							} else if(daysOfWeekState.get(i).equals("2")) {
								daysOfWeekStateStr.setCharAt(1, '1');
							} else if(daysOfWeekState.get(i).equals("3")) {
								daysOfWeekStateStr.setCharAt(2, '1');
							} else if(daysOfWeekState.get(i).equals("4")) {
								daysOfWeekStateStr.setCharAt(3, '1');
							} else if(daysOfWeekState.get(i).equals("5")) {
								daysOfWeekStateStr.setCharAt(4, '1');
							} else if(daysOfWeekState.get(i).equals("6")) {
								daysOfWeekStateStr.setCharAt(5, '1');
							} else if(daysOfWeekState.get(i).equals("7")) {
								daysOfWeekStateStr.setCharAt(6, '1');
							}
						}
					}
					params.add("sessionId", newSessionId);
					params.add("name", name);
					params.add("endDate", daysOfWeekStateEndDate.replaceAll("-", ""));
					params.add("daysOfWeekState", daysOfWeekStateStr.toString());
					params.add("userkey", panoptoAuth.getAuthKey());
					params.add("userpwd", panoptoAuth.getAuthPwd());
					System.err.println(params.toString());
					
					data = new HttpEntity<MultiValueMap<String,Object>>(params, headers);
					
					ResponseEntity<ResultArray> retRecurr = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getRecordingRecurring()), params, ResultArray.class);
					System.err.println(retRecurr.getBody().toString());
					if(!retRecurr.getBody().isBoolResult()) {
						result.put("isBool", false);
						result.put("alert", retRecurr.getBody().getMsg());
					} else {
						params.clear();
						System.err.println(Arrays.toString(retRecurr.getBody().getSessionIDs().toArray()));
						List<String> newSessionIds = retRecurr.getBody().getSessionIDs();
						params.add("sessionIds", newSessionId);
						for(int i=0; i<newSessionIds.size(); i++) {
							params.add("sessionIds", newSessionIds.get(i));
						}
						params.add("userkey", panoptoAuth.getAuthKey());
						params.add("userpwd", panoptoAuth.getAuthPwd());
						System.err.println(params.toString());
						
						ResponseEntity<ResultArray> retView = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleViewByIds()), params, ResultArray.class);
						System.err.println(retView.getBody().toString());
						
						if(retView.getBody().isBoolResult()) {
							String msg = "";
							List<Session> retViewList = retView.getBody().getSession();
							if(retViewList!=null && retViewList.size()>0) {
								for(int i=0; i<retViewList.size(); i++) {
									Session session = (Session)retViewList.get(i);
									msg+="sessionName="+session.getName()+" sessionId="+session.getId()+" sessionViewUrl="+session.getViewerUrl()+"\n";
								}
							}
							result.put("session", msg);
							result.put("isBool", true);
							result.put("alert", "예약되었습니다.");
						} else {
							result.put("session", null);
							result.put("isBool", false);
							result.put("alert", "예약 등록이 실패하였습니다.\n예약 일정이 중복되는지 확인해주세요.");
						}
					}
				} else {
					params.clear();
					params.add("id", newSessionId);
					params.add("userkey", panoptoAuth.getAuthKey());
					params.add("userpwd", panoptoAuth.getAuthPwd());
					System.err.println(params.toString());
					
					ResponseEntity<ResultArray> retView = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleView()), params, ResultArray.class);
					System.err.println(retView.getBody().toString());
					
					result.put("session", "sessionName="+retView.getBody().getSession().get(0).getName() + " sessionId=" +retView.getBody().getSession().get(0).getId() + " sessionViewUrl=" + retView.getBody().getSession().get(0).getViewerUrl());
					result.put("isBool", true);
					result.put("alert", "예약되었습니다.");
				}
			}
		}
		
		return result;
    }
    
	@RequestMapping(value = "/pt/schedule_a_meeting_soon", method= {RequestMethod.GET, RequestMethod.POST})
    public String scheduleAMeetingSoon(Principal principal, Model model,
    		@RequestParam(value="alert", required=false, defaultValue="") String alert,
    		@RequestParam(value="folderOwnerId", required=false, defaultValue="") String folderOwnerId
    		) {
		/*
		 * folderOwnerId 에 로그인 아이디를 넣어서 해당 사용자의 폴더데이터만 가져온다.
		 */
		int pageSize = 1000;
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("currentPage", "1");
		params.add("page_size", pageSize);
		params.add("searchQuery", folderOwnerId);
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		
		ResponseEntity<ResultArray> retF = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getFolderList()), params, ResultArray.class);
		List<Folder> fList = retF.getBody().getFolders();
		
		List<JqTree> folders = new ArrayList<JqTree>(); 
		if(fList!=null && fList.size()>0) {
			for(int i=0; i<fList.size(); i++) {
				Folder f = (Folder)fList.get(i);
				JqTree inDt = new JqTree();
				inDt.setLabel(f.getName());
				inDt.setValue(f.getId());
				if(f.getChildFolders().length > 0) {
					List<JqTree> subfolders = new ArrayList<JqTree>(); 
					inDt.setExpanded(false);
					JqTree items = new JqTree();
					items.setValue(String.join(",", f.getChildFolders()));
					items.setLabel("Loading...");
					items.setItems(null);
					subfolders.add(items);
					inDt.setItems(subfolders);
				} else {
					inDt.setExpanded(false);
				}
				folders.add(inDt);
			}
		}

		System.err.println(folders.toString());
		model.addAttribute("launchUrl", launchUrl);
		model.addAttribute("folders", folders);
		model.addAttribute("alert", alert);
		
        return "panopto/schedule_a_meeting_panopto_soon";
    }
	 
	@RequestMapping(value = "/pt/schedule_a_meeting_soon_save", method= {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody HashMap<String, Object> scheduleAMeetingSoonSave(
    		@RequestParam(value="name2", required=true, defaultValue="") String name2,
    		@RequestParam(value="folderId2", required=true, defaultValue="") String folderId2,
    		@RequestParam(value="description2", required=true, defaultValue="") String description2,
    		@RequestParam(value="isBroadcast2", required=true, defaultValue="false") String isBroadcast2
    		) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("name", name2);
		params.add("folderId", folderId2);
		params.add("isBroadcast", isBroadcast2);
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		
		/*
		 * 한글등록 시 인코딩 설정하기
		 */
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
		HttpEntity<MultiValueMap<String, Object>> data = new HttpEntity<MultiValueMap<String,Object>>(params, headers);
		
		ResponseEntity<Result> retF = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleCreate()), data, Result.class);
		System.err.println("session create=" + retF.getBody().toString());
		 
		if(!retF.getBody().isBoolResult()) { 
			result.put("isBool", false); 
			result.put("alert", "예약이 되지않습니다.");
		} else {
			result.put("session", retF.getBody().getSession());
			result.put("isBool", true);
			result.put("alert", "예약되었습니다.");
			String newSessionId = retF.getBody().getSession().getId();
			
			
			MultiValueMap<String, Object> param_grant = new LinkedMultiValueMap<String, Object>();
			param_grant.add("sessionId", newSessionId);
			param_grant.add("role", "ViewerWithLink");
			param_grant.add("userkey", panoptoAuth.getAuthKey());
			param_grant.add("userpwd", panoptoAuth.getAuthPwd());
			ResponseEntity<String> rets = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getGrantAllAuthenticatedUsersGroupAccessToSession()), param_grant, String.class);
			System.err.println("role session input =>"+rets.getBody().toString());
			
			if(!description2.equals("")) {
				params.clear();
				data = null;

				params.add("sessionId", newSessionId);
				params.add("description", description2);
				params.add("userkey", panoptoAuth.getAuthKey());
				params.add("userpwd", panoptoAuth.getAuthPwd());
				System.err.println(params.toString());
				
				data = new HttpEntity<MultiValueMap<String,Object>>(params, headers);
				
				ResponseEntity<String> retDesc = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleEditDesc()), data, String.class);
				System.err.println(retDesc.getBody().toString());
			}
		}
		
		return result;
    }
	
	@RequestMapping(value = "/pt/report_list", method= {RequestMethod.GET, RequestMethod.POST})
    public String reportlist(
    		Model model,
    		@RequestParam(value="currentPage", required=false, defaultValue="1") int currentPage,
    		@RequestParam(value="staFlag", required=false, defaultValue="") String staFlag,
    		@RequestParam(value="folderId", required=false, defaultValue="") String folderId,
    		@RequestParam(value="sessionId", required=false, defaultValue="") String sessionId,
    		@RequestParam(value="userId", required=false, defaultValue="") String userId,
    		@RequestParam(value="startDate", required=false, defaultValue="") String startDate,
    		@RequestParam(value="endDate", required=false, defaultValue="") String endDate
    		) {
		int pageSize = 10;
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		if(!startDate.equals("")) params.add("startDate", startDate.replaceAll("-", "")+"000000");
		if(!endDate.equals("")) params.add("endDate", endDate.replaceAll("-", "")+"235959");
		params.add("currentPage", currentPage);
		params.add("page_size", pageSize);
//		params.add("folderOwnerId", folderOwnerId);
		/*
		 * sessionName, searchQuery 에는 항상 같은 검색어를 넣어준다.
		 */
//		params.add("sessionName", sessionName);
//		params.add("searchQuery", sessionName);
//		params.add("sessionState", sessionState);
		params.add("sort", "Date");
		params.add("sortIncreasing", "Desc");
		params.add("userkey", panoptoAuth.getAuthKey());
		params.add("userpwd", panoptoAuth.getAuthPwd());
		System.err.println(params.toString());
		/*
		 * 한글검색 시 인코딩 설정하기
		 */
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
		HttpEntity<MultiValueMap<String, Object>> data = new HttpEntity<MultiValueMap<String,Object>>(params, headers);
		
		ResponseEntity<ResultArray> ret = restTemplate.postForEntity(panoptoAuth.getApiUrl(panoptoAuth.getEndpoint(), apiUrl.getScheduleList()), data, ResultArray.class);
		System.err.println(ret.getBody().toString());
		utils.PagingAction page = new utils.PagingAction(
				currentPage, 
				ret.getBody().getTotal(), 
				pageSize,
				10, 
				"searchFrm", 
				"");
		
		model.addAttribute("launchUrl", launchUrl);
		model.addAttribute("list", ret.getBody().getSessions());
//		model.addAttribute("folderOwnerId", folderOwnerId);
//		model.addAttribute("sessionName", sessionName);
//		model.addAttribute("sessionState", sessionState);
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		model.addAttribute("page", page.getPagingHtml());
		model.addAttribute("maxnumber", ret.getBody().getTotal() - ((currentPage - 1) * pageSize));
		
        return "panopto/schedule_list";
    }

}
