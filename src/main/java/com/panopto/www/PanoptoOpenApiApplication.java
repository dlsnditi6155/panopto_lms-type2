package com.panopto.www;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PanoptoOpenApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PanoptoOpenApiApplication.class, args);
	}

}
