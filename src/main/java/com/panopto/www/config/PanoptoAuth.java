package com.panopto.www.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@ConfigurationProperties(prefix = "panopto.auth")
@Data
public class PanoptoAuth {
	private String authKey;
	private String authPwd;
	private String endpoint;
	
	public String getApiUrl(String domain, String endurl) {
		return "http://" + domain + endurl;
	}
}
