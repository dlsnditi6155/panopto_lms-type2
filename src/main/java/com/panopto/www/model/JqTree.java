package com.panopto.www.model;

import lombok.Data;

@Data
public class JqTree {
	private String label;
	private String value;
	private Boolean expanded;
	private Object items;
}
