package com.panopto.www.model;

import java.util.Calendar;

import lombok.Data;

@Data
public class Session {
	private int num;
	private String duration;
	private String duration_real;
	private String id;
	private String name;
	private Object state;
	private String viewerUrl;
	private String goviewerUrl;
	private String folderId;
	private String description;
	private Calendar startTime;
	private String thumbUrl;
	private String strStartTime;
	private String descriptionName;
	private String creatorId;
	private String uploadBt;
}
