package com.panopto.www.model;

import lombok.Data;

@Data
public class User {
	private String firstName;
	private String lastName;
	private String userKey;
	private String userId;
}
