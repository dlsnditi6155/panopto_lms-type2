package com.panopto.www.model;

import lombok.Data;

@Data 
public class Result {
	private boolean boolResult;
	private String Msg;
	private Object obj;
	private Session session;
}
