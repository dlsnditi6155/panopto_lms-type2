package com.panopto.www.model;

import java.util.Calendar;

import lombok.Data;

@Data
public class ScheduledRecordingInfo {
    private Calendar endTime;
    private String sessionID;
    private String sessionName;
    private Calendar startTime;
}
