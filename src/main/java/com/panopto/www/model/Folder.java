package com.panopto.www.model;

import lombok.Data;

@Data
public class Folder {
	private String name;
	private String id;
	private String[] childFolders;
}
