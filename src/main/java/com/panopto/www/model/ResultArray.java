package com.panopto.www.model;

import java.util.List;

import lombok.Data;

@Data
public class ResultArray {
	private boolean boolResult;
	private String msg;
	private Object obj;
	private boolean conflictsExist;
	private List<ScheduledRecordingInfo> sheduledRecordingInfo;
	private int total;
	private List<Session> sessions;
	private List<RemoteRecorder> remoteRecorder;
	private List<Folder> folders;
	private List<Folder> folder;
	private List<String> sessionIDs;
	private List<Session> session;
	private List<User> user;
}
