package utils;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.Date;

public class Utils {
	
	public static String returnDatePT(Date time) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return format.format(time);
	}
	
	public static String getDomain(String viewerUrl, String thumbUrl) {
		try {
			return viewerUrl.substring(0, viewerUrl.indexOf("/Panopto")) + thumbUrl;
		} catch(Exception e) {
			return thumbUrl;
		}
	}

	public static String getSubstring(String str, int cut) {

		if (str != null) {
			StringCharacterIterator iter = null;

			if (str.length() > cut) {
				try {
					// 문자를 8859_1 타입에서 자르고, UTF-8로 인코딩 함
					iter = new StringCharacterIterator(new String(str.substring(0, cut).getBytes("8859_1"), "UTF-8"));

				} catch (UnsupportedEncodingException e) {
				}
				int type = Character.getType(iter.last());
				if (type == Character.OTHER_SYMBOL)
					cut--;
				// 재검사
				iter.setText(str.substring(0, cut));
				type = Character.getType(iter.last());
				if (type == Character.OTHER_SYMBOL)
					cut += 2;

				// 문자를 다시 잘라 리턴
				return str.substring(0, cut) + "...";
			} else {
				return str;
			}

		} else {
			return "";
		}
	}
}
