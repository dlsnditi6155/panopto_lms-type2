var basicDemo = (function () {
    //Adding event listeners
    function _addEventListeners() {
        

        $('.windowButton').click(
            function () {
                if( $("#window").css('display') == 'block') {
                    $('#window').jqxWindow('close');
                }else{
                    $('#window').jqxWindow('open');
                }
            }
        );


    };
    //Creating all page elements which are jqxWidgets
    function _createElements() {
        $("#groupUpload").jqxButton({theme: 'material-green', width: 50 + '%'});
        $("#excelUpload").jqxButton({theme: 'material-green'});
    };
    //Creating the demo window
    function _createWindow() {
        var jqxWidget = $('#jqxWidget');
        var offset = jqxWidget.offset();
        $('#window').jqxWindow({
            position: { x: 30 + '%' , y: 30 + '%'} ,
            showCollapseButton: true, minHeight: 410, height: 'auto', width: 1000,
            resizable: false,
            autoOpen: false,
            showCollapseButton: false,
            showCloseButton: false,
            draggable: true,
            showCloseButton: true,
            initContent: function () {
                $('#window').jqxWindow('focus');
            }
        });
    };
    return {
        config: {
            dragArea: null
        },
        init: function () {
            //Creating all jqxWindgets except the window
            _createElements();
            //Attaching event listeners
            _addEventListeners();
            //Adding jqxWindow
            _createWindow();
        }
    };
} ());
$(document).ready(function () {  
    //Initializing the demo
    basicDemo.init();

    /* 그리드 */
    var data = new Array();
    // generate sample data.
    var generatedata = function (startindex, endindex) {
        var data = {};
        for (var i = startindex; i < endindex; i++) {
            var row = {};
            row["num"] = i + 1;
            row["name"] = ["홍길동"];
            row["email"] = ["abcd123456@naver.com"];
            row["grade"] = ["A"];

            data[i] = row;
        }
        return data;
    }
    var source =
    {
        datatype: "array",
        localdata: {},
        totalrecords: 100
    };
    var rendergridrows = function (params) {
        var data = generatedata(params.startindex, params.endindex);
        return data;
    }
    var dataAdapter = new $.jqx.dataAdapter(source);
    var cellsrenderer = function (row, column, value) {
        return '<div style="text-align: center;">' + value + '</div>';
    }
    var columnrenderer = function (value) {
        return '<div style="text-align: center;">' + value + '</div>';       
    }
    var dataAdapter = new $.jqx.dataAdapter(source);
    $("#grid").jqxGrid(
    {
        pagesize: 20,
        width: 100 + "%",
        height: 'auto',
        autoheight: true, 
        columnsresize: true,
        pagermode: 'simple',
        source: dataAdapter,                
        virtualmode: true,
        pageable: true,
        rendergridrows: rendergridrows,
        selectionmode: 'checkbox',
        altrows: true,
        rowsheight: 26,
        columnsheight: 26,
        theme: 'light',
        columns: [
            { text: "순번", datafield: "num", width: 10 + '%'},
            { text: "이름", datafield: "name", width: 20 + '%'},
            { text: "이메일", datafield: "email", width: 40 + "%"},
            { text: "등급", datafield: "grade", width: 20 + "%"}
        ]
    });

    /* fileupload */
    $('#jqxFileUpload').jqxFileUpload({ width: 980, uploadUrl: 'upload.php', fileInputName: 'fileToUpload', theme: 'material-green' });
    $('#eventsPanel').jqxPanel({ width: 978, height: 150});
    $('#jqxFileUpload').on('select', function (event) {
        var args = event.args;
        var fileName = args.file;
        var fileSize = args.size;
        $('#eventsPanel').jqxPanel('append', '<strong>' + event.type + ':</strong> ' +
            fileName + ';<br />' + 'size: ' + fileSize + '<br />');
    });
    $('#jqxFileUpload').on('remove', function (event) {
        var fileName = event.args.file;
        $('#eventsPanel').jqxPanel('append', '<strong>' + event.type + ':</strong> ' + fileName + '<br />');
    });
    $('#jqxFileUpload').on('uploadStart', function (event) {
        var fileName = event.args.file;
        $('#eventsPanel').jqxPanel('append', '<strong>' + event.type + ':</strong> ' + fileName + '<br />');
    });
    $('#jqxFileUpload').on('uploadEnd', function (event) {
        var args = event.args;
        var fileName = args.file;
        var serverResponce = args.response;
        $('#eventsPanel').jqxPanel('append', '<strong>' + event.type + ':</strong> ' +
            fileName + ';<br />' + 'server response: ' + serverResponce + '<br />');
    });

    

    
    $("#excelDownload_group").jqxButton({theme: 'material-green', width: 50 + '%'});  
    $("#groupAdd").jqxButton({theme: 'material-green'});


    $("#excelDownload_grid").jqxButton({theme: 'material-green'});

    // Create jqxDropDownButton
    $("#dropDownButton").jqxDropDownButton({ width: 200, height: 30, theme: 'material-green'});
    $('#buttonTree').on('select', function (event) {
        var args = event.args;
        var item = $('#buttonTree').jqxTree('getItem', args.element);
        var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + item.label + '</div>';
        $("#dropDownButton").jqxDropDownButton('setContent', dropDownContent);
    });
    $("#buttonTree").jqxTree({ width: 200, theme: 'material-green'});

    $("#groupname_input").jqxInput({placeHolder: "그룹명 입력", height: 29, width: 70 + '%', minLength: 1});
    
    $("#selectMove").jqxButton({theme: 'material-green'});

    $("#sampleDownload").jqxButton({theme: 'material-green', width:150});


});