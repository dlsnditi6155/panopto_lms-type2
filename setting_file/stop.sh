#!/bin/sh


PROC="PANOPTO_OPEN_API.jar"

for PRC in ${PROC}
do
        SH_PID=`ps -ef | grep $PRC | grep -v grep | grep -v vi | awk '{print $2}'`

        if [ -z "$SH_PID" ]; then
            echo "Not Found press $PRC."
        else
            echo "kill process $PRC pid [$SH_PID]"
            kill -9 ${SH_PID}
        fi
done

